package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        SQLiteDatabase BD=dbHelper.getWritableDatabase(); //Obtiene el repositorio de datos en modo de escritura
        ContentValues values= new ContentValues(); //Crea un nuevo mapa de valores, donde los nombres de las columnas son las claves
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE, productName); //Aquí se establece el nombre que contiene los elementos que irán en las filas, es decir el nombre de la columna
        BD.insert(ShoppingElementEntry.TABLE_NAME, null, values); //Inserta las nuevas filas, devolviendo el valor de la clave primaria de la nueva fila
    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query - La Tabla para consultar
            allColumns,                         // The columns to return - Las columnas para retornar
            null,                              // The columns for the WHERE clause - Las columnas para la cláusula WHERE
            null,                           // The values for the WHERE clause - Los valores para la cláusula WHERE
            null,                               // don't group the rows - No agrupa las filas
            null,                                 // don't filter by row groups - No filtra por grupo de filas
            null                                  // The sort order - El orden de clasificacion
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        SQLiteDatabase BD=dbHelper.getWritableDatabase(); //Obtiene el repositorio de datos en modo escritura como arriba en Insertar
        BD.delete(ShoppingElementEntry.TABLE_NAME, null, null); //Especifica mediante una instruccion SQL lo que va a eliminar, en este caso todos los items
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        SQLiteDatabase BD=dbHelper.getWritableDatabase(); //Obtiene el repositorio de datos en modo escritura nuevamente
        ContentValues values=new ContentValues(); //Se Crea un nuevo mapa de valores, donde los nombres de las columnas son las claves
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName()); //Se establece un nuevo nombre para la Columna
        String selection=ShoppingElementEntry._ID + " = ?"; //Especifica las filas con el titulo del producto
        String[] selectionArgs={String.valueOf(shoppingItem.getId())}; //Especifica los datos en el orden del marcador de seleccion
        BD.update(ShoppingElementEntry.TABLE_NAME,values,selection,selectionArgs); //Mediante está instruccion SQL items que se actualizarán
    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        SQLiteDatabase BD=dbHelper.getWritableDatabase(); //Obtiene el repositorio de datos en modo escritura nuevamente
        String selection=ShoppingElementEntry.COLUMN_NAME_TITLE + " = ?"; //Especifica las filas con el titulo del producto
        String[] selectionArgs={shoppingItem.getName()}; //Especifica los datos en el orden del marcador de seleccion
        BD.delete(ShoppingElementEntry.TABLE_NAME,selection,selectionArgs); //Elimina los items especificados de la tabla
    }
}
